# Systems administration

## Automatic updates enabled by default

The base image here enables the
[bootc-fetch-apply-updates.service](https://github.com/containers/bootc/blob/main/manpages-md-extra/bootc-fetch-apply-updates.service.md)
systemd unit which automatically finds updated container images from the
registry and will reboot into them.

### Controlling automatic updates

First, one can disable the timer entirely as part of a container build:

```dockerfile
RUN systemctl mask bootc-fetch-apply-updates.timer
```

This is useful for environments where manually updating the systems is
preferred, or having another tool perform schedule and execute the
updates, e.g. Ansible.

Alternatively, one can use systemd "drop-ins" to override the timer
(for example, to schedule updates for once a week), create a file
like this, named e.g. `50-weekly.conf`:

```systemd
[Timer]
# Clear previous timers
OnBootSec= OnBootSec=1w OnUnitInactiveSec=1w
```

Then add it into your container:

```dockerfile
RUN mkdir -p /usr/lib/systemd/system/bootc-fetch-apply-updates.timer.d
COPY 50-weekly.conf /usr/lib/systemd/system/bootc-fetch-apply-updates.timer.d
```

## Air-gapped and dissconnected updates

For environments without a direct connection to a centralized container
registry, we encourage mirroring an on-premise registry if possible or manually
moving container images using `skopeo copy`.
See [this blog](https://www.redhat.com/sysadmin/manage-container-registries)
for example.

For systems that require manual updates via USB drives, this procedure
describes how to use `skopeo` and `bootc switch`.

Copy image to USB Drive:

```skopeo copy docker://[registry]/[path to image] dir://run/media/$USER/$DRIVE/$DIR```

*note, Using the dir transport will create a number of files,
and it's recommended to place the image in it's own directory.
If the image is local the containers-storage transport will transfer
the image from a system directly to the drive:

```skopeo copy containers-storage:[image]:[tag] dir://run/media/$USER/$DRIVE/$DIR```

From the client system, insert the USB drive and mount it:

```mount /dev/$DRIVE /mnt```

`bootc switch` will direct the system to look at this mount point for future
updates, and is only necessary to run one time if you wish to continue
consuming updates from USB devices. note that if the mount point changes,
simply run this command to point to the alternate location. We recommend
using the same location each time to simplfy this.

```bootc switch --transport dir /mnt/$DIR```

Finally `bootc upgrade` will 1) check for updates and 2) reboot the system
when --apply is used.

```bootc upgrade --apply```

## Filesystem interaction and layout

At "build" time, this image runs the same as any other OCI image where
the default filesystem setup is a writable `overlayfs` for `/` that captures all
changes written - to anywhere.

However, the default runtime (when booted on a virtual or physical host system,
with systemd as pid 1) there are some rules around persistence and writability.

The reason for this is that the primary goal is that base operating system
changes (updating kernels, binaries, configuration) are managed in your container
image and updated via `bootc upgrade`.

In general, aim for most content in your container image to be underneath
the `/usr` filesystem.  This is mounted read-only by default, and this
matches many other "immutable infrastructure" operating systems.

The `/etc` filesystem defaults to persistent and writable - and is the expected
place to put machine-local state (static IP addressing, hostnames, etc).

All other machine-local persistent data should live underneath `/var` by default;
for example, the default is for systemd to persist the journal to `/var/log/journal`.

### Understanding composefs, non /usr directories and mount points

At a technical level today, the base image uses the
[bootc](https://github.com/containers/bootc) project, which uses
[ostree](https://github.com/ostreedev/ostree) as a backend.  However, unlike many
other current ostree-based systems, this base image enables `composefs` by default.

Instead of a single read-only bind mount over `/usr`, the `composefs` root
captures effectively everything from the container except `/etc`, `/var` (mentioned)
above, and the [API filesystems](https://www.freedesktop.org/wiki/Software/systemd/APIFileSystems/).

For example, it is supported to `RUN mkdir /foo` to create an empty directory for a toplevel
mount point.

Additionally, key differences appear around two directories:

### /opt and /usr/local

Because this base image is designed to be used as a base image upon which things
are derived, the `/opt` and `/usr/local` directories are just that - directories.
Content injected into them is "lifecycle bound" with the container image, and
immutable by default.

Particularly for `/opt`, it is not uncommon for software which installs there
to expect to be able to write to certain directories such as `/opt/someisv/log`.
One simple technique to deal with this is to make `/opt/someisv/log` be a symlink
to `/var/log/someisv` instead, and similarly make `/opt/someisv/cache` be a symlink
to `/var/cache/someisv`.

### Returning `/opt` (or `/usr/local` e.g.) to being local state

In some use cases, you may actually want `/opt` or a similar directory
to be *persistent* mutable state, because this was supported on the
original e.g. Container Linux and is inherited
by default into its successors of [Flatcar Linux](flatcar.org) and
[Fedora CoreOS](https://fedoraproject.org/coreos/).

That can be done via e.g. `RUN rmdir /opt && ln -sf /var/opt /opt && mkdir /var/opt`.
(More ideally instead of the `mkdir /var/opt`, inject a systemd [tmpfiles.d](https://www.freedesktop.org/software/systemd/man/latest/tmpfiles.d.html)
 to create `/var/opt` always at boot time)
