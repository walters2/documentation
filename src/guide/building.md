# Configuring systems via container builds

A key part of the idea of this project is that every tool and technique
one knows for building application container images should apply
to building bootable host systems.

There is significant generic guidance in the [bootc upstream documentation](https://containers.github.io/bootc/building/guidance.html).

Most configuration for a Linux system boils down to writing a file (`COPY`)
or executing a command (`RUN`).

## Embedding application containers

A common pattern is to add "application" containers that have references
embedded in the bootable host container.

For example, one can use the [podman systemd](https://docs.podman.io/en/latest/markdown/podman-systemd.unit.5.html)
configuration files, embedded via a container build instruction:

```dockerfile
FROM <base>
COPY foo.container /usr/share/containers/systemd
```

In this model, the application containers will be fetched and run on firstboot.
A key choice is whether to refer to images by digest, or by tag.  Referring
to images by digest ensures repeatable deployments, but requires shipping
host OS updates to update the workload containers.  Referring to images
by tag allows you to use other tooling to dynamically update the workload
containers.

## Configuring systemd units

To add a custom systemd unit:

```dockerfile
COPY mycustom.service /usr/lib/systemd/system
RUN systemctl enable mycustom.service
```

### Using static enablement

Currently, `systemctl enable` will write a symlink to `/etc`.  It
is generally preferred to keep the system state in `/usr`, so if you wish
to comply with that, use instead e.g.:

```
RUN ln -s mycustom.service /usr/lib/systemd/system/default.target.wants
```

instead.

## Logins and users

### Generic images

The base images from this project are "generic/unconfigured" images that
do not have any hardcoded passwords or SSH keys.

### Logging in via dynamic mechanisms

For development/testing purposes, when using local virtualization
it's recommended to inject SSH credentials via e.g. [systemd credentials](https://systemd.io/CREDENTIALS/).
This is the technique used by the [podman-bootc project](https://gitlab.com/bootc-org/podman-bootc-cli)
which injects a SSH key for root this way.

For infrastructure scenarios that do not work with SMBIOS, such as
AWS or VSphere, another approach is to add
`cloud-init` or `vmware-guest-agent` or a similar tool into your image.

### Reconfiguring OpenSSH to use a hardcoded key for root

This fragment in a container image build configures OpenSSH to use
a hardcoded key for the root user, that lives underneath the immutable
`/usr` filesystem.  This ensures that changes to the key propagate
across subsequent OS updates.

```dockerfile
RUN echo 'AuthorizedKeysFile /usr/etc-system/%u.keys' >> /etc/ssh/sshd_config.d/30-auth-system.conf && \
    echo 'ssh-ed25519 AAAAC3Nza... root@example.com' > /usr/etc-system/root.keys && chmod 0600 /usr/etc-system/root.keys
```

### Injecting users in container builds

It is supported to do a basic:

```dockerfile
RUN useradd someuser
```

There is also [an example of injecting a user with SSH key](https://gitlab.com/bootc-org/examples/-/merge_requests/13).

### Injecting users at disk image generation time

It is also supported to inject a user via [Anaconda kickstart](https://pykickstart.readthedocs.io/en/latest/kickstart-docs.html#user)
as well as via [bootc-image-builder](https://github.com/osbuild/bootc-image-builder).

An advantage of doing this at disk image generation time is that it allows
injecting credentials into "generic" images that work in an infrastructure-agnostic
way - i.e. exactly the same across bare metal, cloud, etc.

There is a current subtle difference between these two, which is that changes
to `/etc/passwd` inherently become machine-local state when adding users
via injecting into the disk image.

### Launching unprivileged code via systemd DynamicUser=yes

For many server cases, it's better to structure code as part of a systemd
unit that uses [DynamicUser=yes](https://0pointer.net/blog/dynamic-users-with-systemd.html)
or a container image (by default lesser privileged),
instead of having a traditional unprivileged Unix user.

### Avoiding home directory persistence

In a default installation, the `/root` and `/home` directories are persistent,
and are symbolic links to `/var/roothome` and `/var/home` respectively. This
persistence is typically highly desirable for machines that are somewhat "pet"
like, from desktops to some types of servers, and often undesirable for
scale-out servers and edge devices.

It's recommended for most use cases that don't want a persistent home
directory to inject a systemd unit like this for both these directories,
that uses [tmpfs](https://www.kernel.org/doc/html/latest/filesystems/tmpfs.html):

```systemd
[Unit]
Description=Create a temporary filesystem for /var/home
DefaultDependencies=no
Conflicts=umount.target
Before=local-fs.target umount.target
After=swap.target

[Mount]
What=tmpfs
Where=/var/home
Type=tmpfs
```

If your systems management tooling discovers SSH keys dynamically
on boot (cloud-init, afterburn, etc.) this helps ensure that there's fewer
conflicts around "source of truth" for keys.

### Usage of `ostree container commit`

While you may find `RUN ostree container commit` as part of some
container builds, specifically this project aims to use
`root.transient` which obviates most of the incompatibility
detection done in that command.

In other words it's not needed and as of recently does very little.  We are likely
to introduce a new static-analyzer type process with a different name
and functionality in the future.

## Example repositories

The following git repositories have some useful examples:

- [examples](https://gitlab.com/bootc-org/examples)
- [coreos/layering-examples](https://github.com/coreos/layering-examples)
- [openshift/rhcos-image-layering-examples](https://github.com/openshift/rhcos-image-layering-examples/)
