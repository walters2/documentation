# The Fedora/CentOS bootc project

This project produces a reference base image using
Fedora and CentOS Stream content, suitable for use with the
[bootc project](https://github.com/containers/bootc).

## Why

The original Docker container model of using "layers" to model applications has been extremely successful.
This project aims to apply the same technique for bootable host systems - using standard OCI/Docker
containers as a transport and delivery format for base operating system updates.

The container image includes a Linux kernel (in e.g. /usr/lib/modules), which is used to boot.
At runtime on a target system, the base userspace is not itself running in a container by default. 
For example, assuming systemd is in use, systemd acts as pid1 as usual - there's no "outer" process.

The resulting system has transactional A/B updates.  For more, see [bootc upgrades](https://containers.github.io/bootc/upgrades.html)

## Example use cases

- Edge/embedded bare metal systems that need reliable update and rollback
- "Fixed function" datacenter systems: e.g. nodes in a [Ceph storage cluster](https://docs.ceph.com/en/latest/rados/)
- Base image for Linux desktop systems (e.g. [Universal Blue](https://universal-blue.org/))
- Generic Customized container host (like CoreOS, but also with complete host control)

## Available images (CentOS Stream 9)

- `quay.io/centos-bootc/centos-bootc:stream9` (CentOS Stream 9)

## Available base images (Fedora)

This is in development, see [Fedora bootc](https://gitlab.com/bootc-org/fedora-bootc/fedora-bootc-base) but as
of right now:

- `registry.gitlab.com/bootc-org/fedora-bootc/fedora-bootc-base/fedora-bootc-full:40`
- `registry.gitlab.com/bootc-org/fedora-bootc/fedora-bootc-base/fedora-bootc-minimal:40`


